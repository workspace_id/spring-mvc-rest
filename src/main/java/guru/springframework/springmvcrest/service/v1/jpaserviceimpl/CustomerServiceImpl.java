package guru.springframework.springmvcrest.service.v1.jpaserviceimpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.domain.Count;
import guru.springframework.springmvcrest.domain.Customer;
import guru.springframework.springmvcrest.repositories.CustomerRepository;
import guru.springframework.springmvcrest.service.v1.CountService;
import guru.springframework.springmvcrest.service.v1.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImpl implements CustomerService, CountService {
    private final CustomerRepository customerRepository;
    private final ObjectMapper objectMapper;

    public CustomerServiceImpl(CustomerRepository customerRepository, ObjectMapper objectMapper) {
        this.customerRepository = customerRepository;
        this.objectMapper = objectMapper;
    }

    @Override
    public Customer findById(Long id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public List<Customer> findAll() {
        return customerRepository.findAll();
    }

    @Override
    public Customer save(Customer customer) {
        return customerRepository.save(customer);
    }

    @Override
    public List<Customer> deleteById(Long id) {
        customerRepository.deleteById(id);
        return customerRepository.findAll();
    }












    @Override
    public Customer update(Long id, Customer customer) {
        Customer customerEntry = customerRepository.findById(id).get();
        customerEntry = customer;
        customerEntry.setId(id);
        return customerRepository.save(customerEntry);
    }

    public Customer updateFields(Long id, JsonPatch patch) throws JsonProcessingException, JsonPatchException {
        Customer customerEntry = findById(id);
        JsonNode patched = patch.apply(objectMapper.convertValue(customerEntry, JsonNode.class));
        customerEntry = objectMapper.treeToValue(patched, Customer.class);
        return customerRepository.save(customerEntry);
    }

    @Override
    public Count getCustomerCount() {
        return new Count(customerRepository.count());
    }


}
