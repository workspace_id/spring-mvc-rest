package guru.springframework.springmvcrest.service.v1;

import guru.springframework.springmvcrest.domain.Customer;

import javax.transaction.Transactional;

@Transactional
public interface CustomerService extends CRUDService<Customer> {

}
