package guru.springframework.springmvcrest.service.v1.jpaserviceimpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.domain.Task;
import guru.springframework.springmvcrest.service.v1.CRUDService;

import java.util.List;

public class TaskServiceImpl implements CRUDService<Task> {
    @Override
    public Task findById(Long id) {
        return null;
    }

    @Override
    public List<Task> findAll() {
        return null;
    }

    @Override
    public Task save(Task domainObject) {
        return null;
    }

    @Override
    public List<Task> deleteById(Long id) {
        return null;
    }

    @Override
    public Task update(Long id, Task domainObject) {
        return null;
    }

    @Override
    public Task updateFields(Long id, JsonPatch patch) throws JsonProcessingException, JsonPatchException {
        return null;
    }
}
