package guru.springframework.springmvcrest.service.v1;

import guru.springframework.springmvcrest.domain.Admin;
import guru.springframework.springmvcrest.service.v1.CRUDService;

public interface AdminService extends CRUDService<Admin> {
}
