package guru.springframework.springmvcrest.service.v1;

import guru.springframework.springmvcrest.domain.Count;

public interface CountService {
    Count getCustomerCount();

}
