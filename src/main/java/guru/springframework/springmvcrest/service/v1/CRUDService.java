package guru.springframework.springmvcrest.service.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

import java.util.List;

public interface CRUDService<T> {
    T findById(Long id);
    List<T> findAll();
    T save(T domainObject);
    List<T> deleteById(Long id);
    T update(Long id, T domainObject);

    T updateFields(Long id, JsonPatch patch) throws JsonProcessingException, JsonPatchException;
}
