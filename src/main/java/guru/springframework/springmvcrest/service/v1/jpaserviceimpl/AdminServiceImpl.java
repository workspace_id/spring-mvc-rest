package guru.springframework.springmvcrest.service.v1.jpaserviceimpl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.domain.Admin;
import guru.springframework.springmvcrest.service.v1.AdminService;

import java.util.List;

public class AdminServiceImpl implements AdminService {

    @Override
    public Admin findById(Long id) {
        return null;
    }

    @Override
    public List<Admin> findAll() {
        return null;
    }

    @Override
    public Admin save(Admin domainObject) {
        return null;
    }

    @Override
    public List<Admin> deleteById(Long id) {
        return null;
    }

    @Override
    public Admin update(Long id, Admin domainObject) {
        return null;
    }

    @Override
    public Admin updateFields(Long id, JsonPatch patch) throws JsonProcessingException, JsonPatchException {
        return null;
    }
}
