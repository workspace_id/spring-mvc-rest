package guru.springframework.springmvcrest.service.v1;

import guru.springframework.springmvcrest.domain.Task;

public interface TaskService extends CRUDService<Task>{
}
