package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.springmvcrest.domain.Employee;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class EmployeeCRUDServiceImpl extends AbstractCRUDService {
    public EmployeeCRUDServiceImpl(JpaRepository<Employee, Long> jpaRepository, ObjectMapper objectMapper) {
        super(jpaRepository, objectMapper);
    }
}
