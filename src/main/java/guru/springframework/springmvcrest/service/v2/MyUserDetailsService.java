package guru.springframework.springmvcrest.service.v2;

import guru.springframework.springmvcrest.domain.MyUserDetails;
import guru.springframework.springmvcrest.domain.User;
import guru.springframework.springmvcrest.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MyUserDetailsService implements UserDetailsService {
    UserRepository userRepository;

    public MyUserDetailsService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<User> user = userRepository.findByUsername(s);


        //Checks if user exists or not found
        user.orElseThrow(() ->new UsernameNotFoundException("Not found " + s));
        return user.map(user1 -> new MyUserDetails(user1)).get();
    }
}
