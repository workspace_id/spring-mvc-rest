package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.springmvcrest.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class TaskServiceCRUDImpl extends AbstractCRUDService<Task> {
    public TaskServiceCRUDImpl(JpaRepository<Task, Long> jpaRepository, ObjectMapper objectMapper) {
        super(jpaRepository, objectMapper);
    }
}
