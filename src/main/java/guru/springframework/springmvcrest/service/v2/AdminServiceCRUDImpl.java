package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.springmvcrest.domain.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class AdminServiceCRUDImpl extends AbstractCRUDService<Admin> {
    public AdminServiceCRUDImpl(JpaRepository<Admin, Long> jpaRepository, ObjectMapper objectMapper) {
        super(jpaRepository, objectMapper);
    }
}
