package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.springmvcrest.domain.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public class ManagerCRUDServiceImpl extends AbstractCRUDService<Manager> {
    public ManagerCRUDServiceImpl(JpaRepository<Manager, Long> jpaRepository, ObjectMapper objectMapper) {
        super(jpaRepository, objectMapper);

    }
}
