package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.databind.ObjectMapper;
import guru.springframework.springmvcrest.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceCRUDImpl extends AbstractCRUDService<Customer> {
    public CustomerServiceCRUDImpl(JpaRepository<Customer, Long> jpaRepository, ObjectMapper objectMapper) {
        super(jpaRepository, objectMapper);
    }
}
