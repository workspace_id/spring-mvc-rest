package guru.springframework.springmvcrest.service.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public abstract class AbstractCRUDService<DomainObject> {
    private final JpaRepository<DomainObject, Long> jpaRepository;
    private final ObjectMapper objectMapper;

    private DomainObject domainObjectType;

    private Class<DomainObject> getDomainClass() {
        return (Class<DomainObject>) domainObjectType.getClass();
    }

    public AbstractCRUDService(JpaRepository<DomainObject, Long> jpaRepository, ObjectMapper objectMapper) {
        this.jpaRepository = jpaRepository;
        this.objectMapper = objectMapper;
    }

    public DomainObject findById(Long id) {
        return jpaRepository.findById(id).get();
    }

    public List<DomainObject> findAll() {
        return jpaRepository.findAll();
    }

    public DomainObject save(DomainObject domainObject) {
        return jpaRepository.save(domainObject);
    }

    public List<DomainObject> deleteById(Long id) {
        jpaRepository.deleteById(id);
        return jpaRepository.findAll();
    }












    public DomainObject update(Long id, DomainObject domainObject) {
        DomainObject entry = jpaRepository.findById(id).get();
        entry = domainObject;
        //entry.setId(id);
        return jpaRepository.save(entry);
    }

    public DomainObject updateFields(Long id, JsonPatch patch) throws JsonProcessingException, JsonPatchException {
        DomainObject entry = findById(id);
        JsonNode patched = patch.apply(objectMapper.convertValue(entry, JsonNode.class));
        entry = objectMapper.treeToValue(patched, getDomainClass());
        return jpaRepository.save(entry);
    }






}
