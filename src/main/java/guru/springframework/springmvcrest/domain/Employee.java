package guru.springframework.springmvcrest.domain;

import lombok.Data;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.List;


@Entity
@Data
//Used to have 2 different classes to be saved to the same table
@Inheritance(strategy =  InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "EMPLOYEE_TYPE")
@Table(name = "Employee")
public class Employee {
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    @Id
    //Auto generated private id
    private Long id;
    private String firstName;
    private String lastName;
    private int egn;
    private String email;
    @Column(name="phone_number")
    private String phoneNumber;
    @Column(name="hourly_rate")
    private int hourlyRate;


    @Lob
    //We use @Lob to make the ORM mark this field as CLOB or BLOB - for storing large amounts of information
    //This is as usually a String is 255 characters maximum and our String here can be a lot longer than that
    private String description;

    //One to one relationship with Worksheet
    //Cascade information - passing information
    @OneToOne(cascade = CascadeType.ALL)
    private WorkSheet worksheet;

    //One to many relationship with tasks


    //Property of employee will be stored on the EmployeeTasks Table
    @ManyToMany
    @JoinTable(
            name = "EmployeeTasks",
            joinColumns = @JoinColumn(
                    name = "employee_id",
                    referencedColumnName = "id"
            ),
            inverseJoinColumns = @JoinColumn(
                    name = "task_id",
                    referencedColumnName = "id"
            )
    )
    private List<Task> tasks;

    @OneToMany(mappedBy = "employee")
    private List<Customer> customers;



    //Property of employee will be stored on the employeeTasks Class/Table
    //private List<EmployeeTasks> employeeTasks;



}
