package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    @Column(name="company_name")
    private String companyName;
    private int egn;
    private String email;
    @Column(name="phone_number")
    private String phoneNumber;

    //For Log In
    //roles would be a separate table or this also might be stored in a User table
    private String roles;
    private String password;
    private String active;

    @ManyToOne
    private Employee employee;
    /*@ManyToOne
    private Task task;*/
}
