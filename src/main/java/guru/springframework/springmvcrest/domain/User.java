package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name="User")
@Data
public class User {
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private long id;
    private String username;
    private String password;
    private boolean active;
    private String roles;
    private boolean locked;
    private boolean expired;
    private boolean credentialsExpired;

}
