package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
public class WorkSheet {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long hours;

    //No need to specify a cascade, this is owned by Employee
    //If we delete the worksheet Object, we do not want to delete the Employee Object
    //No cascade operations from this side, but if we delete Employee, we delete their worksheet as well
    @OneToOne
    private Employee employee;
}
