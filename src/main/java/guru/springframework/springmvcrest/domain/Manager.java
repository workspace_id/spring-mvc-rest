package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Manager extends FullTimeEmployee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private int egn;
    private String email;
    @Column(name="phone_number")
    private String phoneNumber;
    @Column(name="salary")
    protected Integer salary;

    String department;

    /*@ManyToOne
    private List<Employee> employees;*/
}
