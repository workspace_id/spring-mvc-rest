package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name ="Task")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "description")
    private String description;
    @Column(name = "is_done")
    private String isDone;
    //private String image;


    /*//One to many relationship with employeeTasks class
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "task")
    private List<EmployeeTasks> employeeTasks;*/

}
