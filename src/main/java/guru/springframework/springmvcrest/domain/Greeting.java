package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
//@Table(name="Greeting")
public class Greeting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    String greeting;

    public Greeting(String greeting) {
        this.greeting = greeting;
    }
}
