package guru.springframework.springmvcrest.domain;

import lombok.Data;

import javax.persistence.Entity;

@Data
public class Count {
    Long count;


    public Count(Long count) {
        this.count = count;
    }
}
