package guru.springframework.springmvcrest.boostrap;

import guru.springframework.springmvcrest.domain.Customer;
import guru.springframework.springmvcrest.domain.User;
import guru.springframework.springmvcrest.repositories.CustomerRepository;
import guru.springframework.springmvcrest.repositories.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class BootstrapData implements CommandLineRunner {
    private final CustomerRepository customerRepository;
    private final UserRepository userRepository;

    public BootstrapData(CustomerRepository customerRepository, UserRepository userRepository) {
        this.customerRepository = customerRepository;
        this.userRepository = userRepository;
    }

    @Override
    public void run(String... args) throws Exception {
        System.out.println("Loading customer Data");

        Customer c1 = new Customer();
        c1.setFirstName("George");
        c1.setLastName("George");
        customerRepository.save(c1);

        Customer c2 = new Customer();
        c2.setFirstName("John");
        c2.setLastName("Peterson");
        customerRepository.save(c2);



        Customer c3 = new Customer();
        c3.setFirstName("Peter");
        c3.setLastName("Johnson");


        customerRepository.save(c3);


        User user = new User();
        user.setUsername("user");
        user.setPassword("pass");
        //Error when no role?
        user.setActive(true);
        user.setRoles("USER");
        user.setExpired(false);
        user.setLocked(false);
        user.setCredentialsExpired(false);
        userRepository.save(user);

        /*user = new User();
        user.setUsername("user1");
        user.setPassword("pass1");
        //Error when no role?
        user.setActive(false);
        user.setRoles("USER");
        user.setExpired(false);
        user.setLocked(false);
        user.setCredentialsExpired(false);
        userRepository.save(user);

        user = new User();
        user.setUsername("user2");
        user.setPassword("pass2");
        //Error when no role?
        user.setActive(true);
        user.setRoles("MANAGER");
        user.setExpired(false);
        user.setLocked(false);
        user.setCredentialsExpired(false);
        userRepository.save(user);*/

        /*Employee e1 = new Employee();
        e1.setFirstName("George");
        e1.setLastName("Peter");*/




        System.out.println("Customers saved: " + customerRepository.count());
    }
}
