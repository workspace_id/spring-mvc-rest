package guru.springframework.springmvcrest.repositories;

import guru.springframework.springmvcrest.domain.Task;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TaskRepository extends JpaRepository<Task, Long> {
}
