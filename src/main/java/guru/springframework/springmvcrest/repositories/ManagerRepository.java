package guru.springframework.springmvcrest.repositories;

import guru.springframework.springmvcrest.domain.Manager;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ManagerRepository extends JpaRepository<Manager, Long> {
}
