package guru.springframework.springmvcrest.repositories;

import guru.springframework.springmvcrest.domain.Admin;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdminRepository extends JpaRepository<Admin, Long> {
}
