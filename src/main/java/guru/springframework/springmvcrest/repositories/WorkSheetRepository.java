package guru.springframework.springmvcrest.repositories;

import guru.springframework.springmvcrest.domain.WorkSheet;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WorkSheetRepository extends JpaRepository<WorkSheet, Long> {
}
