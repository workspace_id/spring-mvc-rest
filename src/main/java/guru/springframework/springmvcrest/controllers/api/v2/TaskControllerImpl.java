package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.Task;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(TaskControllerImpl.BASE_URL)
public class TaskControllerImpl extends AbstractCRUDController<Task> {
    public static final String BASE_URL = "/api/v2/tasks";

    public TaskControllerImpl(AbstractCRUDService<Task> crudService) {
        super(crudService);
    }
}
