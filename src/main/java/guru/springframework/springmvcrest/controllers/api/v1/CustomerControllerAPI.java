package guru.springframework.springmvcrest.controllers.api.v1;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.domain.Customer;
import guru.springframework.springmvcrest.service.v1.CustomerService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


//This can also be called endpoints instead of controllers
//Cross Origin configuration should be modified to be custom where required
@CrossOrigin
@RestController
@RequestMapping(CustomerControllerAPI.BASE_URL)
public class CustomerControllerAPI {

    //When there is a GET request on the BASE_URL, the List<Customer> method will be executed
    public static final String BASE_URL = "/api/v1/customers";
    private final CustomerService customerService;

    public CustomerControllerAPI(CustomerService customerService) {
        this.customerService = customerService;

    }

    //GET Request for all Customers
    @GetMapping
    List<Customer> getAllCustomers() {
        return customerService.findAll();
    }


    //GET Request for a single customer
    @GetMapping("/{id}")
    public Customer getCustomerById(@PathVariable Long id) {
        return customerService.findById(id);
    }

    //POST Request for adding a new customer
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer saveCustomer(@RequestBody Customer customer) {
        return customerService.save(customer);
    }



    //PUT Request for updating a customer
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public Customer updateCustomer(@PathVariable("id") Long id, @RequestBody Customer customer) {
        return customerService.update(id, customer);

    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public Customer updateCustomerFields(@PathVariable Long id, @RequestBody JsonPatch patch) throws JsonPatchException, JsonProcessingException {
            return customerService.updateFields(id, patch);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<Customer> deleteCustomer(@PathVariable("id") Long id) {
        return customerService.deleteById(id);

    }






}
