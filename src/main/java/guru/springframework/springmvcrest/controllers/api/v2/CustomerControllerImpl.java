package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.Customer;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

//This can also be called endpoints instead of controllers
//Cross Origin configuration should be modified to be custom where required
@CrossOrigin
@RestController
@RequestMapping(CustomerControllerImpl.BASE_URL)
public class CustomerControllerImpl extends AbstractCRUDController<Customer> {
    public static final String BASE_URL = "/api/v2/customers";

    public CustomerControllerImpl(AbstractCRUDService<Customer> crudService) {
        super(crudService);
    }





    /*@PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer save(@RequestBody customer) {
        return super.getCrudService().save(customer);

    }
*/


}



