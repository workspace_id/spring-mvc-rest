package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.WorkSheet;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(WorkSheetControllerImpl.BASE_URL)
public class WorkSheetControllerImpl extends AbstractCRUDController<WorkSheet> {
    public static final String BASE_URL = "/api/v2/worksheets";

    public WorkSheetControllerImpl(AbstractCRUDService<WorkSheet> crudService) {
        super(crudService);
    }
}
