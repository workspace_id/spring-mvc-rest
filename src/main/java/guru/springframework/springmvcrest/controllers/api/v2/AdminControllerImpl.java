package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.Admin;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(AdminControllerImpl.BASE_URL)
public class AdminControllerImpl extends AbstractCRUDController<Admin> {
    public static final String BASE_URL = "/api/v2/admins";

    public AdminControllerImpl(AbstractCRUDService<Admin> crudService) {
        super(crudService);
    }

}
