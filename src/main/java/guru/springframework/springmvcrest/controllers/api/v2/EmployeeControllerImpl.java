package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.Employee;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(EmployeeControllerImpl.BASE_URL)
public class EmployeeControllerImpl extends AbstractCRUDController<Employee> {
    public static final String BASE_URL = "/api/v2/employees";

    public EmployeeControllerImpl(AbstractCRUDService<Employee> crudService) {
        super(crudService);
    }
}
