package guru.springframework.springmvcrest.controllers.api.v2;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Data
public abstract class AbstractCRUDController<DomainObject> {
    private final AbstractCRUDService<DomainObject> crudService;

    public AbstractCRUDController(AbstractCRUDService<DomainObject> crudService) {
        this.crudService = crudService;
    }

    //GET Request for all domainObjects
    @GetMapping
    List<DomainObject> getAll() {
        return crudService.findAll();
    }


    //GET Request for a single domainObject
    @GetMapping("/{id}")
    public DomainObject getById(@PathVariable Long id) {
        return crudService.findById(id);
    }

    //POST Request for adding a new domainObject
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DomainObject save(@RequestBody DomainObject domainObject) {
        return crudService.save(domainObject);
    }



    //PUT Request for updating a domainObject
    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public DomainObject update(@PathVariable("id") Long id, @RequestBody DomainObject domainObject) {
        return crudService.update(id, domainObject);

    }

    @PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
    public DomainObject updateFields(@PathVariable Long id, @RequestBody JsonPatch patch) throws JsonPatchException, JsonProcessingException {
            return crudService.updateFields(id, patch);
    }


    @DeleteMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public List<DomainObject> delete(@PathVariable("id") Long id) {
        return crudService.deleteById(id);
    }

    /*@GetMapping(value = "/count")
    public Count getCount() {
        return crudService.getCount();
    }*/






}
