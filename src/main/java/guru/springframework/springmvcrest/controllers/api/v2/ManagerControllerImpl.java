package guru.springframework.springmvcrest.controllers.api.v2;

import guru.springframework.springmvcrest.domain.Manager;
import guru.springframework.springmvcrest.service.v2.AbstractCRUDService;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping(ManagerControllerImpl.BASE_URL)
public class ManagerControllerImpl extends AbstractCRUDController<Manager> {
    public static final String BASE_URL = "/api/v2/managers";

    public ManagerControllerImpl(AbstractCRUDService<Manager> crudService) {
        super(crudService);

    }
}
