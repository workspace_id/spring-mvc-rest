package guru.springframework.springmvcrest.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class IndexControllerImpl {

    @GetMapping("/")
    public ModelAndView loadWelcomePage() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index.html");
        return modelAndView;
    }

    @GetMapping("/user")
    public String user() {
        return ("<h1>Welcome user</h1>");
    }

    @GetMapping("/admin")
    public String admin() {
        return ("<h1>Welcome admin</h1>");
    }

    @GetMapping("/employee")
    public String employee() {
        return ("<h1> Welcome employee </h1>");



    }
}
