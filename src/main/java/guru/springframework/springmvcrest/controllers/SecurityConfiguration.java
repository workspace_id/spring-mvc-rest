package guru.springframework.springmvcrest.controllers;

import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    //Authentication manager (our code) talks to authentication providers (our or not our code?) and that talks to the user details service -> loads user by username, returns User Object using username
    //It really does not matter if it is JPA or a text file or etc.
    //You have to create the user details service on your  own
    UserDetailsService userDetailsService;

    public SecurityConfiguration(UserDetailsService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        super.configure(auth);
        //For JDBC there is out of the box authentication
        //For JPA there is no standard out of the box authentication
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                //.antMatchers("/api/v2/employees/").hasRole("Admin")



                //Authorize only if user has role admin
                .antMatchers("/admin").hasRole("ADMIN")
                //Authorize only if user has role admin or user
                .antMatchers("/user").hasAnyRole("ADMIN", "USER")

                .antMatchers("/employee").hasAnyRole("MANAGER", "EMPLOYEE")
                .antMatchers("/customer").hasAnyRole("CUSTOMER")
                //Permit anyone to enter this url
                .antMatchers("/").permitAll()
                .and().formLogin();
    }




    @Bean
    public PasswordEncoder getPasswordEncoder() {
        //No encoding with this class, this is just an example
        //There should be encoding
        return NoOpPasswordEncoder.getInstance();
    }



}
