package guru.springframework.springmvcrest.controllers.api.v2;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.event.annotation.AfterTestClass;
import org.springframework.test.context.event.annotation.BeforeTestClass;

import static org.junit.jupiter.api.Assertions.*;

public class CustomerControllerAPITest {
    @BeforeTestClass
    void beforeClass() {
        System.out.println("Before Class");
    }

    @Before("test1")
    void before() {
        System.out.println("Before");
    }

    @After("test1")
    void after() {
        System.out.println("After");
    }

    @AfterTestClass
    void afterClass(){
        System.out.println("After Class");
    }



    @Test
    void CRUDWorks() {
        assert 1 == 1;
    }

    @Test
    void test1() {
        String[] array1 = new String[0];
        String[] array2 = new String[0];
        boolean boolean1 = true;
        boolean boolean2 = false;
        int result = 5;
        assert result == 5;
        Object object1 = new Object();
        assertEquals(6, 6);
        assertEquals(true, boolean1);
        assertNotNull(object1);
        assertTrue(boolean1);
        assertFalse(boolean2);
        object1 = null;
        assertNull(object1);
        assertArrayEquals(array1, array2);

    }
}
