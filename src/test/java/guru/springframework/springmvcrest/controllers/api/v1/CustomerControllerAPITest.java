package guru.springframework.springmvcrest.controllers.api.v1;

import guru.springframework.springmvcrest.domain.Customer;
import guru.springframework.springmvcrest.repositories.CustomerRepository;
import guru.springframework.springmvcrest.service.v1.CustomerService;
import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class CustomerControllerAPITest {
    //Adds a mock web context
    //No Tomcat or etc. required to run tests
    //No Spring Context required
    MockMvc springMvcRestApplication;

    @InjectMocks
    CustomerControllerAPI customerControllerAPI;

    @Mock
    CustomerService customerService;

    @Mock
    CustomerRepository customerRepository;

    @BeforeAll
    public void before() {
        MockitoAnnotations.initMocks(this);
        springMvcRestApplication = MockMvcBuilders.standaloneSetup(customerControllerAPI).build();
        Customer c1 = new Customer();
        c1.setFirstName("George");
        c1.setLastName("George");
        Customer c2 = new Customer();
        customerRepository.save(c1);
        //ArrayList<Customer> resultList = new ArrayList<Customer>();
        //resultList.add(c1);
        //when(customerRepository.findAll()).thenReturn(resultList);
    }

    @Test
    public void testListItems() throws Exception {
        springMvcRestApplication.perform(get("/api/v1/customers/"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].firstName", is("George")));
    }

    @Test
    public void testPost() throws Exception {
        springMvcRestApplication.perform(post("/api/v1/customers/")
                .content("{\"firstName\": \"Peter\", \"lastName\": \"Johnson\"}")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)));
    }






    @Test
    public void testCustomer() throws Exception {
        springMvcRestApplication.perform(get("/api/v1/customers/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.firstName", is("George")));
    }



}
